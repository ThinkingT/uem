package uem

import "sync"

type RefCounter struct {
	ref int
	m   sync.Mutex
}

func (r *RefCounter) Set(value int) {
	r.m.Lock()
	defer r.m.Unlock()
	r.ref = value
}
func (r *RefCounter) Get() int {
	return r.ref
}
func (r *RefCounter) Decrement() {
	r.m.Lock()
	defer r.m.Unlock()
	r.ref--
}
func (r *RefCounter) Increment() {
	r.m.Lock()
	defer r.m.Unlock()
	r.ref++
}
