package uem

import (
	"net/http"
	_ "net/http/pprof"
)

func ProcessNotify(addr string) {

	//http://127.0.0.1:10000/debug/pprof/
	Info("Process is Notify is %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		Error("start pprof failed on %s\n", addr)
	}
}
