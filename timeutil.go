package uem

import (
	"time"
)

// UnixTimestamp 将 time.Time 转换为 Unix 时间戳（秒）
func UnixTimestamp(t time.Time) int64 {
	return t.Unix()
}

// UnixNanoTimestamp 将 time.Time 转换为 Unix 时间戳（纳秒）
func UnixNanoTimestamp(t time.Time) int64 {
	return t.UnixNano()
}

// CurrentUnixTimestamp 获取当前时间的 Unix 时间戳（秒）
func CurrentUnixTimestamp() int64 {
	return time.Now().Unix()
}

// CurrentUnixNanoTimestamp 获取当前时间的 Unix 时间戳（纳秒）
func CurrentUnixNanoTimestamp() int64 {
	return time.Now().UnixNano()
}

// StringToUnixTimestamp 将时间字符串（遵循RFC3339格式）转换为 Unix 时间戳（秒）
// 注意：这里简化了错误处理，实际使用时应该添加
func StringToUnixTimestamp(timeStr string) (int64, error) {
	t, err := time.Parse(time.RFC3339, timeStr)
	if err != nil {
		return 0, err
	}
	return t.Unix(), nil
}

// UnixTimestampToString 将 Unix 时间戳（秒）转换为时间字符串（遵循RFC3339格式）
func UnixTimestampToString(timestamp int64) string {
	return time.Unix(timestamp, 0).Format(time.RFC3339)
}

// AddDuration 将给定的 duration 加到 t 上，并返回新的 Unix 时间戳（秒）
func AddDuration(t time.Time, duration time.Duration) int64 {
	return t.Add(duration).Unix()
}

// SubtractDuration 从 t 中减去给定的 duration，并返回新的 Unix 时间戳（秒）
func SubtractDuration(t time.Time, duration time.Duration) int64 {
	return t.Add(-duration).Unix()
}

// 示例用法
//func main() { // 注意：main 函数通常不应该放在包级别的文件中，这里只是为了演示
//	now := time.Now()
//	future := AddDuration(now, 3600*time.Second) // 现在时间加上1小时
//	past := SubtractDuration(now, 3600*time.Second) // 现在时间减去1小时
//
//	fmt.Println("Current Unix Timestamp:", now.Unix())
//	fmt.Println("Future Unix Timestamp (1 hour later):", future)
//	fmt.Println("Past Unix Timestamp (1 hour ago):", past)
//}
