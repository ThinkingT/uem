package uem

import (
	"os"
	"path/filepath"
	"strings"
)

func GetCurrentPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
	}
	return strings.Replace(dir, "\\", "/", -1)
}

func GetFileToDirectory(path string) string {

	return filepath.Dir(path)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
