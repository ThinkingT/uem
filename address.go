package uem

import "fmt"

func ToAddress(host string, port string) string {
	addr := fmt.Sprintf("%s:%s", host, port)
	return addr
}

func ToAddr(host string, port int) string {
	addr := fmt.Sprintf("%s:%d", host, port)
	return addr
}
func ToUrlAddr(host string, port int) string {
	addr := fmt.Sprintf("http://%s:%d", host, port)
	return addr
}
