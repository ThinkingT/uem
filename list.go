package uem

import (
	"container/list"
	"sync"
)

type List struct {
	lt   list.List
	lock sync.Mutex
}

func NewList() *List {
	return &List{}
}

func (p *List) AddNode(v interface{}) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.lt.PushBack(v)

}
func (p *List) DelNode(v interface{}) {
	p.lock.Lock()
	defer p.lock.Unlock()
}

func (p *List) AllList(f func(value interface{})) {

	for item := p.lt.Front(); nil != item; item = item.Next() {
		f(item.Value)
	}
}
