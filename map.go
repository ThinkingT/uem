package uem

import (
	"errors"
	"sync"
)

type Map struct {
	mp sync.Map
}

func NewMap() *Map {
	return &Map{}
}

func (p *Map) SetValue(key interface{}, v interface{}) {

	p.mp.Store(key, v)
}

func (p *Map) GetValue(key interface{}) (interface{}, error) {

	value, ok := p.mp.Load(key)
	if ok {
		return value, nil
	}
	return nil, errors.New("no found ")
}
func (p *Map) DeleteValue(key interface{}) {
	p.mp.Delete(key)
}

func (p *Map) IsValue(key interface{}) bool {
	_, ok := p.mp.Load(key)
	return ok
}
func (p *Map) len() {
	
}
func (p *Map) Rang(f func(key, value interface{}) bool) {
	p.mp.Range(f)
}
