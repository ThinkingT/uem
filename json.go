package uem

import (
	"encoding/json"
)

func ConvertObj(msg []byte, obj interface{}) error {

	if err := json.Unmarshal(msg, &obj); err != nil {
		Error("json to obj  faild :%s", err)
		return err
	}
	return nil
}
func ConvertMsg(obj interface{}) ([]byte, error) {

	msg, err := json.Marshal(obj)
	if err != nil {
		Error("obj to json faild :%s", err)
		return msg, err
	}
	return msg, nil
}
