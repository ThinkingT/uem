package uem

import "fmt"

type Net struct {
	Port int    `toml:"port"`
	IP   string `toml:"ip"`
} //`toml:"net"`

func (c *Net) GetNetAddr() string {
	return fmt.Sprintf("%s:%d", c.IP, c.Port)
}

type Mysql struct {
	Username string `toml:"username"`
	Password string `toml:"password"`
	Charset  string `toml:"charset"`
	IP       string `toml:"ip"`
	Port     int    `toml:"port"`
	Dbname   string `toml:"dbname"`
} //`toml:"mysql"`

func (c *Mysql) GetMySqlAddr() string {

	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=true",
		c.Username,
		c.Password,
		c.IP,
		c.Port,
		c.Dbname,
		c.Charset)
}

type Redis struct {
	IP       string `toml:"ip"`
	Port     int    `toml:"port"`
	Password string `toml:"password"`
	Consize  int    `toml:"consize"`
	Timeout  int64  `toml:"timeout"`
	Network  string `toml:"network"`
	Index    int    `toml:"index"`
} //`toml:"redis"`

func (c *Redis) GetRedisAddr() string {
	return fmt.Sprintf("%s:%d", c.IP, c.Port)
}

type Monitor struct {
	Addr string `toml:"addr"`
} //`toml:"monitor"`

func (c *Monitor) GetMonitorAddr() string {

	return c.Addr
}

type Appmode struct {
	Mod string `toml:"mod"`
} //`toml:"appmode"

func (c *Appmode) GetAppmode() string {
	return c.Mod
}

type Heart struct {
	Name string `toml:"name"`
} //`toml:"heart

func (c *Heart) GetHeart() string {
	return c.Name
}

type License struct {
	Code     string `toml:"code"`
	Validate int64  `toml:"validate"`
	Session  string `toml:"session"`
} //`toml:"license"`

type API struct {
	Conf string `toml:"conf"`
} //`toml:"api"`

func (c *API) GetAPIFile() string {
	return c.Conf
}
