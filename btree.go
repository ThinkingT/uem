package uem

import "sync"

import (
	"github.com/google/btree"
)

type Btree struct {
	tree *btree.BTree
	lock sync.RWMutex
}

func all(t *btree.BTree) (out []btree.Item) {
	t.Ascend(func(a btree.Item) bool {
		//fmt.Println(a.(*PeerId).index)
		out = append(out, a)
		return true
	})
	return
}

func allrev(t *btree.BTree) (out []btree.Item) {
	t.Descend(func(a btree.Item) bool {
		out = append(out, a)
		return true
	})
	return
}

func NewBtree() *Btree {
	p := new(Btree)
	p.tree = btree.New(1024 * 1024)
	return p
}

func (p *Btree) SetValue(key interface{}) {

	for v := 0; v < 50; v++ {
		p.tree.ReplaceOrInsert(btree.Int(v))
	}
	for v := 100; v > 50; v-- {
		p.tree.ReplaceOrInsert(btree.Int(v))
	}
	var got []btree.Item
	p.tree.AscendGreaterOrEqual(btree.Int(50), func(a btree.Item) bool {
		got = append(got, a)
		return true
	})
}
func (p *Btree) GetValue(key interface{}) interface{} {
	return p.tree.Get(key.(btree.Item))
}
