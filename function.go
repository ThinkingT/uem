package uem

import "reflect"

func IsFunctionExists(class interface{}, name string) bool {

	getValue := reflect.ValueOf(class)
	methodValue := getValue.MethodByName(name)
	args := make([]reflect.Value, 0)
	methodValue.Call(args)
	return true
}
