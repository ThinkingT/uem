package uem

import (
	"time"
)

type TimeController struct {
	TcChannal *time.Ticker
	ticState  bool
	TmChannal *time.Timer
	tmState   bool
	Tsec      time.Duration
}

func NewTimerController() *TimeController {
	return &TimeController{}
}

func (this *TimeController) CreateTimeController(tmsec time.Duration) {
	var activetm time.Duration
	activetm = tmsec * time.Second
	this.TmChannal = time.NewTimer(activetm)
	this.tmState = true

}
func (this *TimeController) UpdateTimeController(tmsec time.Duration) bool {

	var activetm time.Duration
	activetm = tmsec * time.Second

	if !this.TmChannal.Stop() {
		return this.TmChannal.Reset(activetm)
	}
	return false
}

func (this *TimeController) DelTimeController() bool {

	//看官方文档把
	if !this.TmChannal.Stop() {
		select {
		case <-this.TmChannal.C: //try to drain from the channel
		default:
		}

	}
	this.tmState = false
	return true
}

func (this *TimeController) CreateTickController(tmsec time.Duration) {
	var activetm time.Duration
	activetm = tmsec * time.Second
	this.TcChannal = time.NewTicker(activetm)
}

func (this *TimeController) DelTickController() bool {

	this.TcChannal.Stop()
	return true
}

func Close_TimeController(f func()) chan struct{} {
	done := make(chan struct{}, 1)
	go func() {
		timer := time.NewTicker(5 * time.Minute)
		defer timer.Stop()
		for {
			select {
			case <-timer.C:
				f()
			case <-done:
				return
			}
		}
	}()
	return done
}

func (this *TimeController) WaitTimerClose() {

}
