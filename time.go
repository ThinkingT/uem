package uem

import "time"

func GetNowUnixSec() int64 {

	return time.Now().Unix()
}
