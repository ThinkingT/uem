package uem

import (
	"os"
	"sync"
)

type Semp struct {
	wg    sync.WaitGroup
	Ref   RefCounter
	count int
}

func NewSemp(ref int) *Semp {
	ms := new(Semp)
	ms.wg.Add(ref)
	ms.Ref.Set(ref)
	ms.count = ref
	return ms
}
func (p *Semp) Wait() {

	for j := 0; j < p.count; j++ {
		p.wg.Wait()
	}
}
func (p *Semp) Signal() {
	p.Ref.Decrement()
	p.wg.Done()
}
func (p *Semp) Kill() {
	for loop := p.Ref.Get(); loop > 0; loop-- {
		p.wg.Done()
	}
}
func (p *Semp) Exit() {
	os.Exit(1)
}

func (p *Semp) GetRef() int {
	return p.Ref.Get()
}
